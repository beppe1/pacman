﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class UiScore : MonoBehaviour
{
    [SerializeField]
    PacManMovement pacman;
    [SerializeField]
    Text _text;

    private void Awake()
    {
        _text = GetComponent<Text>();
    }

    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        _text.text = pacman.TotalPoints.ToString();
    }
}
